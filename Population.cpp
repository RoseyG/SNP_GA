#include <algorithm>
#include <time.h>
#include "Population.hpp"

Population::Population()
{
    populationSize = 0;
    numNeurons = 0;
}

Population::Population(uint8_t nNeurons) : numNeurons(nNeurons)
{
    populationSize = 0;
}

Population::~Population()
{

}

void Population::populate(std::fstream& pFile)
{
    std::string line;
    while (std::getline(pFile, line))
    {
        Chromosome temp;
        std::string topo;
        std::string oss;
        int fitness;
        // std::getline(pFile, line);
        std::istringstream ss(line);
        ss >> topo;
        ss >> oss;
        ss >> fitness;
        if(topo.empty())
        {
            break;
        }
        temp.setTopology(topo);
        temp.setOutputSpikes(oss);
        temp.setFitness(fitness);
        chromosomes.push_back(temp);
        // std::cout << temp << std::endl;
        populationSize++;
    }
}

void Population::checkFitness(std::fstream& pFile, std::fstream& sFile, std::string eSpikes)
{
    Chromosome temp;
    std::string topo;
    std::string oss;
    while (std::getline(pFile, topo))
    {
    //     std::getline(pFile, topo);
        if(topo.empty())
        {
            break;
        }
        std::getline(sFile, oss);
        temp.setTopology(topo);
        temp.setOutputSpikes(oss);
        temp.calcFitness(eSpikes);
        std::cout << temp << std::endl;
        auto it = std::find_if(chromosomes.begin() + populationSize, chromosomes.end(), [&] (Chromosome c) { return c.getFitness() == temp.getFitness(); });
        if(it != chromosomes.end())
        {
            continue;
        }
        else
        {
            chromosomes.push_back(temp);
        }
    }
}

void Population::select()
{
    std::sort(chromosomes.begin(), chromosomes.end());
    chromosomes.erase(chromosomes.begin() + populationSize, chromosomes.end());
}

void Population::cross()
{   srand(time(0));
    int offset = (rand()%(populationSize - 1)) + 1;
    int n = numNeurons + 1;
    for (int x = 0; x < populationSize; x++)
    {
        Chromosome temp;
        std::string newTopo = "";
        std::string topo1 = chromosomes[x].getTopology();
        std::string topo2 = chromosomes[(x+offset)%populationSize].getTopology();
        // std::cout << "Crossing " << topo1 << " and " << topo2 << std::endl;
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                if (i == 0)
                {
                    newTopo += char(topo1[(i*n)+j]);
                }
                else
                {
                    if (j == n-1)
                    {
                        newTopo += char(topo1[(i*n)+j]);
                    }
                    else
                    {
                        if( ((i*n) + j) % 2 )
                        {
                            newTopo += char(topo1[(i*n)+j]);
                        }
                        else
                        {
                            newTopo += char(topo2[(i*n)+j]);
                        }
                    }
                }
            }
        }
        // std::cout << "New topology: " << newTopo << std::endl;
        temp.setTopology(newTopo);

        auto it = std::find_if(chromosomes.begin(), chromosomes.end(), [&] (Chromosome c) { return c.getTopology() == temp.getTopology(); });
        if(it == chromosomes.end())
        {
            if(temp.isValid(numNeurons))
            {
                chromosomes.push_back(temp);
            }
        }
        else
        {
            std::cout << "Topology already exists." << std::endl;
        }
    }
}

void Population::mutate()
{
    bool mutate = true;
    srand(time(0));
    int n = numNeurons + 1;
    for (int x = populationSize; x < chromosomes.size(); x++)
    {
        Chromosome temp = chromosomes[x];
        std::string newTopo = temp.getTopology();
        // mutate = rand()%2;
        if(mutate)
        {
            // std::cout << "Mutating " << newTopo << std::endl;

            int synapseNum = rand() % (n*numNeurons);
            int i = (synapseNum / n) + 1;
            int j = synapseNum % n;
            if(newTopo[(i*n) + j] == '0')
            {
                newTopo[(i*n) + j] = '1';
            }
            else
            {
                newTopo[(i*n) + j] = '0';
            }
            temp.setTopology(newTopo);
            // std::cout << "New topology: " << temp.getTopology() << std::endl;

            auto it = std::find_if(chromosomes.begin(), chromosomes.end(), [&] (Chromosome c) { return c.getTopology() == temp.getTopology(); });
            if(it == chromosomes.end())
            {
                if(temp.isValid(numNeurons))
                {
                    chromosomes[x] = temp;
                }
            }
        }
        // else
        // {
        //     std::cout << "Not mutating " << newTopo << std::endl;
        // }
    }
    // for (Chromosome temp: chromosomes)
    // {
    //     std::cout << temp << std::endl;
    // }
}

void Population::recordOld(std::fstream& pFile)
{
    int offset = 0;
    for (int offset = 0; offset < populationSize; offset++)
    {
        Chromosome temp = chromosomes[offset];
        pFile << chromosomes[offset].getTopology() << " " << chromosomes[offset].getOutputSpikes() << " " << chromosomes[offset].getFitness() << std::endl;
    }
}

void Population::recordNew(std::fstream& pFile)
{
    int offset = populationSize;
    for (int offset = populationSize; offset < chromosomes.size(); offset++)
    {
        if (chromosomes[offset].isValid(numNeurons))
        {
            pFile << chromosomes[offset].getTopology() << std::endl;
        }
    }
}
