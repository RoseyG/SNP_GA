
#include <iostream>
#include <fstream>
#include "Population.hpp"
#include "Chromosome.hpp"

int main()
{
    std::fstream        populationData;
    std::fstream        simulationData;
    std::fstream        configurationData;
    const std::string   oldPopulation = "DATA/oldPopulation.in";
    const std::string   newPopulation = "DATA/newPopulation.in";
    const std::string   simulationFile = "DATA/simulationOutput.in";
    const std::string   configurationFile = "DATA/cfg.txt";
    std::string         inputSpikes;
    std::string         expectedSpikes;
    int numNeurons;

    std::cout << "Reading "<< configurationFile << std::endl;
    configurationData.open(configurationFile);
    if(configurationData.is_open())
    {
        std::getline(configurationData, inputSpikes);
        std::getline(configurationData, expectedSpikes);
        configurationData >> numNeurons;
        // configurationData.close();
        // std::cout << "Input spikes: " << inputSpikes << std::endl;
        // std::cout << "Expected spikes: " << expectedSpikes << std::endl;
        // std::cout << "Number of neurons: " << numNeurons << std::endl;
    }
    else
    {
        std::cout << "Error opening "<< configurationFile << std::endl;
    }

    Population population(numNeurons);

    std::cout << "Reading "<< oldPopulation << std::endl;
    populationData.open(oldPopulation);
    if(populationData.is_open())
    {
        population.populate(populationData);
        populationData.close();
    }
    else
    {
        std::cout << "Error opening "<< oldPopulation << std::endl;
    }

    std::cout << "Reading "<< newPopulation << " and " << simulationFile << std::endl;
    populationData.open(newPopulation);
    simulationData.open(simulationFile);
    if(simulationData.is_open())
    {
        population.checkFitness(populationData, simulationData, expectedSpikes);
        populationData.close();
        simulationData.close();
    }
    else
    {
        std::cout << "Error opening files" << std::endl;
    }

    std::cout << "Selecting chromosomes for crossover" << std::endl;
    population.select();

    std::cout << "Crossing over chromosomes" << std::endl;
    population.cross();

    std::cout << "Mutating new chromosomes" << std::endl;
    population.mutate();

    std::cout << "Writing to "<< oldPopulation << std::endl;
    populationData.open(oldPopulation, std::ios::out | std::ios::trunc);
    if(populationData.is_open())
    {
        population.recordOld(populationData);
        populationData.close();
    }
    else
    {
        std::cout << "Error opening "<< oldPopulation << std::endl;
    }

    std::cout << "Writing to "<< newPopulation << std::endl;
    populationData.open(newPopulation, std::ios::out | std::ios::trunc);
    if(populationData.is_open())
    {
        population.recordNew(populationData);
        populationData.close();
    }
    else
    {
        std::cout << "Error opening "<< newPopulation << std::endl;
    }

    return 0;
}