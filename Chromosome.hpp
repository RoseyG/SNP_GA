#pragma once

#include <iostream>
#include <sstream>
#include <string>

class Chromosome
{
public:
    Chromosome();
    ~Chromosome();

    bool operator<(Chromosome const &other) {
        return fitness > other.fitness;
    }

    friend std::ostream &operator << (std::ostream &os, Chromosome const &m) {
        return std::cout << m.topology << " " << m.outputSpikes << " " << m.fitness;
    }

    void setTopology(std::string topo);
    void setFitness(int fit);
    void setOutputSpikes(std::string oss);
    void calcFitness(std::string ess);
    bool isValid(int8_t nNeurons);
    std::string getTopology();
    std::string getOutputSpikes();
    int getFitness();
private:
    std::string topology;
    std::string outputSpikes;
    int     fitness = 0;
};