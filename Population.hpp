#pragma once

#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include "Chromosome.hpp"

class Population
{
public:
    Population();
    Population(uint8_t nNeurons);
    ~Population();
    void populate(std::fstream& pFile);
    void checkFitness(std::fstream& pFile, std::fstream& sFile, std::string oss);
    void select();
    void cross();
    void mutate();
    void recordOld(std::fstream& pFile);
    void recordNew(std::fstream& pFile);
private:
    std::vector<Chromosome> chromosomes;
    int                     numNeurons;
    uint8_t                 populationSize;
};