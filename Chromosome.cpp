#include <vector>
#include "Chromosome.hpp"

Chromosome::Chromosome()
{
	topology = "";
	outputSpikes = "";
	fitness = 0;
}

Chromosome::~Chromosome()
{

}

void Chromosome::setTopology(std::string topo)
{
	topology = topo;
}

void Chromosome::setFitness(int fit)
{
    fitness = fit;
}

void Chromosome::setOutputSpikes(std::string oss)
{
	outputSpikes = oss;
}

void Chromosome::calcFitness(std::string ess)
{
    // std::cout << "Comparing: " << ess << " and " << outputSpikes << std::endl;
    fitness = 0;
    int essLen = ess.length();
    int ossLen = outputSpikes.length();
    int ptr = 1;
    for (int i = (ossLen - essLen)+1; i < ossLen; i +=2) 
    {
        if(outputSpikes[i] == ess[ptr])
        {
            // ess[ptr]?fitness++:fitness+=2;
            fitness++;
            ptr+=2;
        }
        // std::cout << ess[essLen-i] << " == " << outputSpikes[ossLen-i] << " Fitnes: " << fitness << std::endl;
    }
}

// void Chromosome::calcFitness(std::string ess)
// {
//     // std::cout << "Comparing: " << ess << " and " << outputSpikes << std::endl;
//     fitness = 0;
//     int essLen = ess.length();
//     int ossLen = outputSpikes.length();
//     for (int i = 1; i < ossLen; i +=2) 
//     {
//         if(outputSpikes[(ossLen - essLen)+i] == ess[i])
//         {
//             ess[i]?fitness++:fitness+=2;
//         }
//         // std::cout << ess[essLen-i] << " == " << outputSpikes[ossLen-i] << " Fitnes: " << fitness << std::endl;
//     }
// }

std::string Chromosome::getTopology()
{
    return topology;
}

std::string Chromosome::getOutputSpikes()
{
    return outputSpikes;
}

int Chromosome::getFitness()
{
    return fitness;
}

bool Chromosome::isValid(int8_t nNeurons)
{
    std::vector<bool> inputConnection;
    std::vector<bool> outputConnection;
    // std::vector<bool> hasConnection;
    int n = nNeurons + 1;
    int inCtr = 0;
    int outCtr = 0;

    for (int i = 0; i < nNeurons; i++)
    {
        inputConnection.push_back(false);
        outputConnection.push_back(false);
        // hasConnection.push_back(false);
        if (topology[i] == '1')
        {
            inputConnection[i] = true;
            inCtr++;
        }
        if (topology[((i+1)*n) + nNeurons] == '1')
        {
            outputConnection[i] = true;
            outCtr++;
        }
        // for (int j = 0; j < nNeurons ; j++)
        // {
        //     if (topology[((i+1)*n) + j] == '1')
        //     {
        //         hasConnection[j] = true;
        //     }
        // }
    }
    if (inCtr != 1)
    {
        // std::cout << "Not valid, there has to be one input neuron." << std::endl;
        return false;
    }
    // if (outCtr > 2)
    // {
    //     std::cout << "Not valid, there has to be no more than two output neurons." << std::endl;
    //     return false;
    // }
    // if (outCtr < 1)
    // {
    //     std::cout << "Not valid, there has to be at least one output neuron." << std::endl;
    //     return false;
    // }

    bool change = true;
    while(change)
    {
        change = false;
        for (int i = 0; i < nNeurons; i++)
        {
            if(inputConnection[i])
            {
                for (int j = 0; j < nNeurons; j++)
                {
                    if (topology[(i+1)*n + j] == '1')
                    {
                        if(!inputConnection[j])
                        {
                            inputConnection[j] = true;
                            change = true;
                        }
                    }
                }
            }

            if(!outputConnection[i])
            {
                for (int j = 0; j < nNeurons; j++)
                {
                    if (topology[(i+1)*n + j] == '1')
                    {
                        if(outputConnection[j])
                        {
                            outputConnection[i] = true;
                            change = true;
                        }
                    }
                }
            }
        }
    }
    for(int i = 0; i < nNeurons; i++)
    {
        // std::cout<< inputConnection.at(i) << " "  << outputConnection.at(i) << " " << hasConnection.at(i) << std::endl;
        // if ((inputConnection.at(i) && outputConnection.at(i)) != hasConnection.at(i))
        // std::cout<< inputConnection.at(i) << " "  << outputConnection.at(i) << std::endl;
        // if ((inputConnection.at(i) != outputConnection.at(i)))
        // {
        //     std::cout << "Not valid, neuron " << i+1 << " is a dangling neuron." << std::endl;
        //     return false;
        // }
        if ((inputConnection.at(i) == outputConnection.at(i)))
        {
            // std::cout << "Path from input to output exists." << std::endl;
            return true;
        }
    }
//     std::cout << "Valid chromosome." << std::endl;
//     return true;
        // std::cout << "Path from input to output does not exist." << std::endl;
        return false;
}